#	Specs

1. En premier lieu, on récupère les coordonnés GPS de l'ESP.
 - On pourra se contenter de coordonnées écrites en dur dans un premier temps.
2. Ensuite, on envois une requete pour récupérér la liste des parkings.
 - On pourra se contenter de données écrites en dur dans un premier temps.
 - On parse le résultat CSV et on stocke dans un tableau de parking_t.
3. Pour chaque parking on récupère ces informations dans une structure parking_data_t.
 - Instancier un client https,
 - On construit la route (http) vers la ressource du parking contenant la date, le nom, le status, le nombre de place restantes et le nb total,
 - Faire une requete GET,
 - Traiter/Transorfmer le resultat (string) en objet de la structure parking_data_t,
 - On filtre sur les deux conditions suivantes :
  - Les parkings ayant des places disponibles.
  - Les parkings ouverts.
 - On calcule la distance entre la coordonnée GPS courante et celle de l'objet parking_t courant :
  - soit par vol d'oiseau (formule mathématique du plan),
  - soit en utilisant l'API OSRM : [http://router.project-osrm.org/route/v1/driving/longitudeSource,latitudeSource;longitudeDestination,lattitudeDestination](http://router.project-osrm.org/route/v1/driving/longitudeSource,latitudeSource;longitudeDestination,lattitudeDestination).
 - On sauvegarde dans un tableau de type parking_distance_t le résultat,
 - Retourner le tableau des distances calculées.
4. On trie le tableau des distances calculées par distance,
 - On peut trié les résultats par nb de place disponible (décroissant).
5. On affiche le tableau trié des parkings par distance.
 - On affiche une liste des parkings avec la distance le séparent du parking et son nombre de place.