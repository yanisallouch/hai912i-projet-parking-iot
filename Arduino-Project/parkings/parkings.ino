#include "parkings.h"
#include <sstream>
#include <iostream>
#include <cstdio>
#include <string>

#include <Arduino.h>

#include <WiFi.h>
#include <WiFiMulti.h>

#include <HTTPClient.h>

#include <WiFiClientSecure.h>

#include <ArduinoJson.h>

using namespace std;
using std::cout; using std::cin;
using std::endl; using std::string;

#define STRING(num) #num

#define LONGITUDE 3.886985
#define LATITUDE 43.613217

#define MONTPELLIER3M_BASE_URL "https://data.montpellier3m.fr/"
#define MONTPELLIER3M_API_PATH_PREFIX "sites/default/files/ressources/"
#define MONTPELLIER3M_API_PATH_SUFFIX ".xml"

WiFiMulti WiFiMulti;

const parking_t parkings[] = {
                             /* ID,           name,                      longitude,         latitude */
                             { "FR_MTP_ANTI",  "Antigone",                   3.888818, 43.608716 },
                             { "FR_MTP_COME",  "Comédie",                    3.879761, 43.608560 },
                             { "FR_MTP_CORU",  "Corum",                      3.882257, 43.613888 },
                             { "FR_MTP_EURO",  "Europa",                     3.892530, 43.607849 },
                             { "FR_MTP_FOCH",  "Foch Préfecture",            3.876570, 43.610749 },
                             { "FR_MTP_GAMB",  "Gambetta",                   3.871374, 43.606951 },
                             { "FR_MTP_GARE",  "Saint Roch",                 3.878550, 43.603291 },
                             { "FR_MTP_TRIA",  "Triangle",                   3.881844, 43.609233 },
                             { "FR_MTP_ARCT",  "Arc de Triomphe",            3.873200, 43.611002 },
                             { "FR_MTP_PITO",  "Pitot",                      3.870191, 43.612244 },
                             { "FR_MTP_CIRC",  "Circé Odysseum",             3.917849, 43.604953 },
                             { "FR_MTP_SABI",  "Sabines",                    3.860224, 43.583832 },
                             { "FR_MTP_GARC",  "Garcia Lorca",               3.890715, 43.590985 },
                             { "FR_CAS_SABL",  "Notre Dame de Sablassou",    3.922295, 43.634191 },
                             { "FR_MTP_MOSS",  "Mosson",                     3.819665, 43.616237 },
                             { "FR_STJ_SJLC",  "Saint-Jean-le-Sec",          3.837931, 43.570822 },
                             { "FR_MTP_MEDC",  "Euromédecine",               3.827723, 43.638953 },
                             { "FR_MTP_OCCI",  "Occitanie",                  3.848597, 43.634562 },
                             { "FR_CAS_CDGA",  "Charles de Gaulle",          3.897762, 43.628542 },
                             { "FR_MTP_ARCE",  "Arceaux",                    3.867490, 43.611716 },
                             { "FR_MTP_POLY",  "Polygone",                   3.884765, 43.608370 },
                             { "FR_MTP_GA109", "Multiplexe (est)",           3.918980, 43.605060 },
                             { "FR_MTP_GA250", "Multiplexe (ouest)",         3.914030, 43.604000 },
                             // Les parkings ci-après n'ont pas de données temps réel sur le site de Montpellier 3M
                             { 0,              "Peyrou",                     3.870383, 43.611297 },
                             { 0,              "Hôtel de ville",             3.895853, 43.599231 },
                             { 0,              "Jacou",                      3.912884, 43.654598 },
                             { 0,              "Georges Pompidou",           3.921084, 43.649339 },
                             { 0,              "Via Domitia",                3.929538, 43.646658 },
                             { 0,              "Juvignac",                   3.809621, 43.617403 },
                             { 0,              "Saint-Jean-de-Védas Centre", 3.830585, 43.574962 },
                             { 0,              "Lattes",                     3.904817, 43.570809 },
                             { 0,              "Parc expo",                  3.945678, 43.572910 },
                             { 0,              "Pérols centre",              3.957355, 43.565378 },
                             { 0,              "Décathlon",                  3.923800, 43.606185 },
                             { 0,              "Ikéa",                       3.925582, 43.604609 },
                             { 0,              "Géant Casino",               3.922104, 43.603155 },
                             { 0,              "Mare Nostrum",               3.919015, 43.602370 },
                             { 0,              "Végapolis",                  3.914773, 43.602896 },
                             { 0,              "Multiplexe",                 3.914110, 43.604152 },
                             { 0,              "La Mantilla",                3.902399, 43.598772 },
                             { 0, 0, 0, 0 }
  };

void setup(){
    Serial.begin(115200);
  // Serial.setDebugOutput(true);

  Serial.println();
  Serial.println();
  Serial.println();

  for (uint8_t t = 4; t > 0; t--) {
    // Serial.printf("[SETUP] WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }

  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("NoWiFi", "yanisahmed");

  // wait for WiFi connection
  Serial.print("En attente de la connexion au WiFi...");
  while ((WiFiMulti.run() != WL_CONNECTED)) {
  }
  Serial.println("WiFi connecté");
}

location_t getCurrentLocation(){
  location_t currentLocation;
  currentLocation.longitude = LONGITUDE;
  currentLocation.latitude = LATITUDE;
  return currentLocation;
}

void displayParkings(const parking_t* parkings){
  cout << "Affichage des parkings disposant d'un ID" << endl;
  const parking_t* ptr = parkings;
  while (ptr->id) {
    cout << "Parking '" << ptr->name << "'"
         << " (ID: " << ptr->id << ")"
         << " Longitude " << ptr->longitude << ", Latitude " << ptr->latitude
         << endl;
    ++ptr;
  }
  cout << endl;
}

void displayParkingsDistance(const parking_distance_t* parkings){
  cout << "Affichage des parkings :" << endl;
  const parking_distance_t* ptr = parkings;
  while (ptr->name) {
    cout << "Parking '" << ptr->name << "'"
         << " (FreePlace : " << ptr->freePlace << ")"
         << " Distance " << ptr->distance << endl;
    ++ptr;
  }
  cout << endl;
}

int getParkingsSize(const parking_t* parkings){
  int cpt = 0;
  const parking_t* ptr = parkings;
  while (ptr->id) {
    cpt++;
    ++ptr;
  }
  return cpt;
}

/**
 * Convertit une string de c++ vers la classe String défini dans Arduino.h
 */
String convertToStringArduino(string aStr){
  String result;
  for(int i = 0; i < aStr.size(); i++){
    result += aStr.at(i);
  }
  return result;
}

String _buildURL(const char *id) {
  string res = MONTPELLIER3M_BASE_URL;
  res += (MONTPELLIER3M_API_PATH_PREFIX);
  res += (id);
  res += (MONTPELLIER3M_API_PATH_SUFFIX);
  String result = convertToStringArduino(res);
  return result;
}

char* getRoutesAsJson(String url){
  char* result;

  WiFiClientSecure *client = new WiFiClientSecure;
  if(client) {
    client->setInsecure();
    {
      // Add a scoping block for HTTPClient https to make sure it is destroyed before WiFiClientSecure *client is 
      HTTPClient https;
  
      // Serial.print("[HTTPS] begin...\n");
      if (https.begin(*client, url)) {  // HTTPS
        // Serial.print("[HTTPS] GET...\n");
        // start connection and send HTTP header
        int httpCode = https.GET();
  
        // httpCode will be negative on error
        if (httpCode > 0) {
          // HTTP header has been send and Server response header has been handled
           // Serial.printf("[HTTPS] GET... code: %d\n", httpCode);
  
          // file found at server
          if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
            String payload = https.getString();
            // Serial.println(payload);
            int payloadLength = payload.length();
            payloadLength++;
            char buf[payloadLength];
            payload.toCharArray(buf, payloadLength);
            result = (char*)malloc(sizeof(char)*payloadLength);
            strcpy(result,buf);
          }
        } else {
           // Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
          result = "";
        }
  
        https.end();
      } else {
         // Serial.printf("[HTTPS] Unable to connect\n");
        result = "";
      }

      // End extra scoping block
    }
  
    delete client;
  } else {
    // Serial.println("Unable to create client");
    result = "";
  }
  return result;
}

/**
   * Instancier un client https,
   * On construit la route (http) vers la ressource du parking contenant la date, le nom, le status, le nombre de place restantes et le nb total,
   * Faire une requete GET,
   */
string getParkingDataXML(const char *id){
  string result = "";

  WiFiClientSecure *client = new WiFiClientSecure;
  if(client) {
    client->setInsecure();
    {
      // Add a scoping block for HTTPClient https to make sure it is destroyed before WiFiClientSecure *client is 
      HTTPClient https;
  
      // Serial.print("[HTTPS] begin...\n");
      String uri = _buildURL(id);
      if (https.begin(*client, uri)) {  // HTTPS
        // Serial.print("[HTTPS] GET...\n");
        // start connection and send HTTP header
        int httpCode = https.GET();
  
        // httpCode will be negative on error
        if (httpCode > 0) {
          // HTTP header has been send and Server response header has been handled
          // Serial.printf("[HTTPS] GET... code: %d\n", httpCode);
  
          // file found at server
          if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
            String payload = https.getString();
            // Serial.println(payload);
            int payloadLength = payload.length();
            payloadLength++;
            char buf[payloadLength];
            payload.toCharArray(buf, payloadLength);
            result = string(buf);
          }
        } else {
          // Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
          result = "";
        }
  
        https.end();
      } else {
        // Serial.printf("[HTTPS] Unable to connect\n");
        result = "";
      }

      // End extra scoping block
    }
  
    delete client;
  } else {
    Serial.println("Unable to create client");
    result = "";
  }
  return result;
}

// Very quick and dirty XML parsing
string _getXmlValue(const string &line, const string &tag) {
  string res = "";
  size_t b1 = line.find_first_of('<');
  if (b1 != string::npos) {
    size_t e1 = line.find_first_of('>', b1);
    if ((e1 != string::npos) && (e1 - b1 - 1 == tag.length()) && !line.compare(b1 + 1, e1 - b1 - 1, tag)) {
      size_t e2 = line.find_last_of('>');
      if (e2 != string::npos) {
        size_t b2 = line.find_last_of('<', e2);
        if ((b2 != string::npos) && (e2 - b2 - 1 == (tag.length() + 1))
            && (line[b2 + 1] == '/') && !line.compare(b2 + 2, e2 - b2 - 2, tag)) {
          res = line.substr(e1 + 1, b2 - e1 - 1);
        }
      }
    }
  }
  return res;
}

/**
 * Transorfmer le resultat (string) en objet de la structure parking_data_t
 */
parking_data_t convertRawToParkingData(string parkingRaw){
  parking_data_t result;
  istringstream f(parkingRaw);
  string line;
  String tmpString;
  while (getline(f, line)) {
    string l = _getXmlValue(line, "DateTime");
    if (!l.empty()) {
      result.date_time = l;
    } else {
      l = _getXmlValue(line, "Status");
      if (!l.empty()) {
        result.open = (l == "Open");
      } else {
        l = _getXmlValue(line, "Free");
        if (!l.empty()) {
          tmpString = convertToStringArduino(l);
          result.free = tmpString.toInt();
        } else {
          l = _getXmlValue(line, "Total");
          if (!l.empty()) {
            tmpString = convertToStringArduino(l);
            result.total = tmpString.toInt();
          }
        }
      }
    }
  }
  return result;
}

float getDistanceFromJson(char* json) {
  DynamicJsonDocument doc(2048);
  DeserializationError error = deserializeJson(doc, json);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return -1;
  }
  double distance = doc["routes"][0]["distance"];

  doc.clear();
  return (float) distance ;
}

float distance(float longitudeA, float latitudeA, float longitudeB, float latitudeB) {
  float result = 0.0;
  ostringstream myString;
  string strConcat = "https://router.project-osrm.org/route/v1/driving/";
  myString << strConcat;
  myString << longitudeA;
  myString << ",";
  myString << latitudeA;
  myString << ";";
  myString << longitudeB;
  myString << ",";
  myString << latitudeB;
  myString << "?overview=false";
  String url = convertToStringArduino(myString.str());
  char* json_result = getRoutesAsJson(url);
  result = getDistanceFromJson(json_result);
  return result;
}

parking_distance_t createParkingDistance(const parking_t* parking, const parking_data_t parkingData){
  parking_distance_t result;
  if(parkingData.open && parkingData.free > 0){
    result.name = parking->name;
    result.freePlace = parkingData.free;
    location_t currentLocation = getCurrentLocation();
    float tmpDistance = distance(currentLocation.longitude, currentLocation.latitude, parking->longitude, parking->latitude);
    if(tmpDistance==-1){
      result.name = 0;
      result.freePlace = 0;
      result.distance = 0;
    }else{
      result.distance = tmpDistance;
    }
  }else{
    result.name = 0;
    result.freePlace = 0;
    result.distance = 0;
  }
  return result;
}

list_parking_distance_t* getParkingsDistance(const parking_t* parkings){
  int nbParkings = getParkingsSize(parkings);
  nbParkings++; // add one case for null end array
  list_parking_distance_t * listResults = (list_parking_distance_t*)malloc(sizeof(list_parking_distance_t));
  listResults->parkingsDistance = (parking_distance_t*)malloc(sizeof(parking_distance_t)*nbParkings);
  const parking_t* ptr = parkings;
  unsigned int indice = 0;
  string currentParkingRaw;
  parking_data_t currentParkingData;
  parking_distance_t tmp;
  while (ptr->id) {
    currentParkingRaw = getParkingDataXML(ptr->id);
    if(!currentParkingRaw.empty()){
      currentParkingData = convertRawToParkingData(currentParkingRaw);
      tmp = createParkingDistance(ptr, currentParkingData);
      if(tmp.name != 0){
        listResults->parkingsDistance[indice].name = tmp.name;
        listResults->parkingsDistance[indice].freePlace = tmp.freePlace;
        listResults->parkingsDistance[indice].distance = tmp.distance;
        indice++;
      } 
    }
    ++ptr;
  }
  listResults->size = indice;
  return listResults;
}

void copyFromTo(parking_distance_t parkingDistance2, parking_distance_t *parkingDistance1){
  parkingDistance1->name = parkingDistance2.name;
  parkingDistance1->freePlace = parkingDistance2.freePlace;
  parkingDistance1->distance = parkingDistance2.distance;
}

parking_distance_t getParkingDistanceMin(list_parking_distance_t listParkingsDistance){
  float min = -1;
  parking_distance_t parkingDistanceMin;
  unsigned int size = listParkingsDistance.size;
  unsigned int indice = 0;
  for(int i =0; i < size; i++){
    if(min==-1){
      copyFromTo(listParkingsDistance.parkingsDistance[i], &parkingDistanceMin);
      min = listParkingsDistance.parkingsDistance[i].distance;
      indice = i;
    }else if (listParkingsDistance.parkingsDistance[i].distance >0 
      && listParkingsDistance.parkingsDistance[i].distance < min){
      copyFromTo(listParkingsDistance.parkingsDistance[i], &parkingDistanceMin);
      min = listParkingsDistance.parkingsDistance[i].distance;
      indice = i;
    }
  }
  listParkingsDistance.parkingsDistance[indice].distance = -1;
  return parkingDistanceMin;
}

void sortParkingsByDistance(list_parking_distance_t* listParkingsDistance){

  cout << "Trie des parkings par distance" << endl;
  unsigned int size = listParkingsDistance->size;
  parking_distance_t parkingDistanceMin;
  list_parking_distance_t listTmp[size];
  listTmp->parkingsDistance = (parking_distance_t*) malloc(sizeof(parking_distance_t)*size);
  for(int i = 0; i < size; i++){
    parkingDistanceMin = getParkingDistanceMin(*listParkingsDistance);
    copyFromTo(parkingDistanceMin, &(listTmp->parkingsDistance[i]));
  }
  for(int i = 0; i < size; i ++){
    copyFromTo(listTmp->parkingsDistance[i], &(listParkingsDistance->parkingsDistance[i]));
  }
}


void loop(){
  location_t currentLocation = getCurrentLocation();
  cout << "Récupération des coordonnées GPS..." << endl;
  cout << "Longitude : " << currentLocation.longitude << ", Latitude : " << currentLocation.latitude << endl;
  const parking_t* parkingPtr = parkings;

  cout << "Récupération des données depuis Open-Data 3M et OSRM..." << endl;
  list_parking_distance_t *listParkingsDistance = getParkingsDistance(parkingPtr);
  sortParkingsByDistance(listParkingsDistance);
  displayParkingsDistance(listParkingsDistance->parkingsDistance);

  Serial.println();
  Serial.println("Waiting 10min before the next round...");

  delay(600000);
}